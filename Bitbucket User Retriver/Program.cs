﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading;

namespace Bitbucket_User_Retriver
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> listaDeUtilizadores = new List<string>();
            DateTime dataDeUtilizacaoDaAPI = new DateTime();
            TimeSpan diferenca = new TimeSpan();
            List<string> listaDeUtilizadoresDoFicheiro = new List<string>();
            int opcaoSelecionada = 0;
            string caminhoDoFicheiro = null;
            string caminhoDaAPI = "https://api.bitbucket.org/2.0/users/";
            string diretorioDoFicheiroLog = Directory.GetCurrentDirectory();
            diretorioDoFicheiroLog = diretorioDoFicheiroLog.Substring(0, (diretorioDoFicheiroLog.LastIndexOf("Retriver") + 9));
            diretorioDoFicheiroLog = diretorioDoFicheiroLog + "file.log";
            HttpClient cliente = new HttpClient();
            HttpResponseMessage pedido = null;
            string respostaDaAPI = null;
            string input;
            Menu();
            void Menu()
            {
                Console.WriteLine("1- Receive file");
                Console.WriteLine("2- Get information about each user");
                Console.WriteLine("3- Exit the application");
                Console.WriteLine();
                Console.WriteLine("Type your option number and press enter: ");
                input = Console.ReadLine();
                validacaoDaOpcaoSelecionada();
                if (opcaoSelecionada == 1)
                {
                    ReceberFicheiro();
                    Console.WriteLine();
                    Menu();
                }
                else if (opcaoSelecionada == 2)
                {
                    ObterInformacaoSobreCadaUtilizador();
                    Thread.Sleep(6000);
                    Console.WriteLine();
                    Menu();
                }
                else if(opcaoSelecionada==3)
                {
                    System.Environment.Exit(0);
                }
            }
            //Método para ler um ficheiro e guardar os utilizadores na listaDeUtilizadores
            //1 - Verificar se o ficheiro existe
            //2 - Ler todas as linhas dentro do ficheiro e verificar se o ficheiro está vazio
            //3 - Verificar se algum dos utilizadores do ficheiro já está guardado na lista de utilizadores já está na listaDeUtilizadores. Se um utilizador não estiver na listaDeUtilizadores é adicionado
            void ReceberFicheiro()
            {
                Console.WriteLine();
                Console.WriteLine("Type your file path and press enter: ");
                caminhoDoFicheiro = Console.ReadLine();
                if (File.Exists(caminhoDoFicheiro) != true)
                {
                    Console.WriteLine();
                    Console.WriteLine("File not found");
                    Console.WriteLine();
                    ReceberFicheiro();
                }
                listaDeUtilizadoresDoFicheiro = File.ReadAllLines(caminhoDoFicheiro).ToList();

                if (listaDeUtilizadoresDoFicheiro.Count() == 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("The file on the path is empty please provide a path to a file with at least 1 user");
                    Console.WriteLine();
                    ReceberFicheiro();
                }
                foreach (string utilizador in listaDeUtilizadoresDoFicheiro)
                {
                    if (listaDeUtilizadores.Contains(utilizador) == false)
                    {
                        listaDeUtilizadores.Add(utilizador);
                    }
                }
            }

            //Método para fazer pedidos a api do Bitbucket e guardar o log de resposta da API num ficheiro log
            //1 - Verificar se já existe algum utilizador na listaDeUtilizadores
            //2 - Verificar se a api foi utilizada a 1 minuto atras
            //3 - Verificação se o ficheiro log está criado e se não estiver é criado
            //4 - Obtenção de informação da api por cada utilizador na listaDeUtilizadores com um intervalo de 5 segundos entre cada um
            //5 - Alterar a data da ultima ultima vez que a api foi utilizada
            //6 - limpar a listaDeUtilizadores
            async void ObterInformacaoSobreCadaUtilizador()
            {
                if (listaDeUtilizadores.Count() == 0)
                {
                    Console.WriteLine();
                    Console.WriteLine("You have to give a file with at least one user before selecting this option");
                    Console.WriteLine();
                }
                if (dataDeUtilizacaoDaAPI.Year>1)
                {
                    diferenca = DateTime.Now.Subtract(dataDeUtilizacaoDaAPI);
                    if (diferenca.TotalSeconds < 60)
                    {
                        Console.WriteLine();
                        Console.WriteLine("You have to wait " + (60 - diferenca.TotalSeconds) + " seconds before selecting this option");
                        Console.WriteLine();
                        Menu();
                    }
                }
                if (File.Exists(diretorioDoFicheiroLog) != true)
                {
                    File.Create(diretorioDoFicheiroLog);
                }
                foreach (string utilizador in listaDeUtilizadores)
                {
                    pedido = await cliente.GetAsync(caminhoDaAPI + "alfasoftdev");
                    Console.WriteLine();
                    Console.WriteLine("user: " + utilizador);
                    Console.WriteLine("url: " + caminhoDaAPI + utilizador);

                    if (pedido.IsSuccessStatusCode)
                    {
                        respostaDaAPI = await pedido.Content.ReadAsStringAsync();
                        Console.WriteLine("response: " + respostaDaAPI);
                        File.AppendAllText(diretorioDoFicheiroLog, utilizador + "_" + caminhoDaAPI + utilizador + "_" + DateTime.Now + ": " + respostaDaAPI + "\n");
                    }
                    else
                    {
                        Console.WriteLine("User " + utilizador + " not found");
                        File.AppendAllText(diretorioDoFicheiroLog, utilizador + "_" + caminhoDaAPI + utilizador + "_" + DateTime.Now + ": User not found" + "\n");
                    }
                    Thread.Sleep(5000);
                    Console.WriteLine();
                }
                dataDeUtilizacaoDaAPI = DateTime.Now;
                listaDeUtilizadores = new List<string>();
            }

            //Método para validar se a o primeiro dado inserido depois de  mostrar o menu é 1 ou 2 ou 3
            //1 - Verifica se o dado que foi inserido é um número inteiro
            //2 - Verifica se o número passado é 1,2 ou 3
            void validacaoDaOpcaoSelecionada()
            {
                if (int.TryParse(input, out opcaoSelecionada)==true)
                {
                    opcaoSelecionada = int.Parse(input);
                    if (opcaoSelecionada!=1&& opcaoSelecionada != 2 && opcaoSelecionada != 3)
                    {
                        Console.WriteLine();
                        Console.WriteLine("You have to write 1 our 2 our 3");
                        Console.WriteLine();
                        Menu();
                    }
                }
                else
                {
                    Console.WriteLine();
                    Console.WriteLine("You have to write 1 our 2 our 3");
                    Console.WriteLine();
                    Menu();
                }
            }
        }
    }
}
